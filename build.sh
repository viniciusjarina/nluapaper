#!/bin/bash

# compile LaTeX
# $1 main latex file without extension

function compileLaTeX()
{
	pdflatex -interaction=nonstopmode $1.tex
	bibtex $1.aux
	makeindex $1.idx 
	makeindex $1.nlo -s nomencl.ist -o $1.nls
	pdflatex -interaction=nonstopmode $1.tex
	pdflatex -interaction=nonstopmode $1.tex
}


# generate LaTeXFiles
function buildPdf()
{
	echo "Compiling artigo"
	compileLaTeX artigo

	
	echo "removing abnTeX2 files" 
	rm -rf *.cls
	rm -rf *.sty
	rm -rf *.bst
	rm -rf *.log
	rm -rf *.nls
	rm -rf *.ilg
	rm -rf *.ind
	rm -rf *.toc
	rm -rf *.lot
	rm -rf *.nlo
	rm -rf *.nls
	rm -rf *.blg
	rm -rf *.bbl
	rm -rf *.lof
	rm -rf *.idx
	rm -rf *.brf
	rm -rf *.aux
}

# build ZIP files
function buildAll()
{
	# compile latex
	buildPdf
}


# MAIN
buildAll

# ending...
