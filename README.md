NLua Paper README.md
=================

This is the NLua Paper project.

+ Pre-requisites:
  
    + pdflatex
    + abntex2

To build the pdf just use:


    ./build.sh


Vinicius Jarina (viniciusjarina@gmail.com)


